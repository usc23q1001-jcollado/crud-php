<?php

// Database connection
$host = 'localhost';
$user = 'root';
$password = '';
$database = 'sqlPractice';

$conn = new mysqli($host, $user, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['create'])) {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $middle_name = $_POST['middle_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    $sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address)
            VALUES ('$first_name', '$last_name', '$middle_name', '$birthday', '$address');";

    if ($conn->query($sql) === TRUE) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Read
$sql = "SELECT * FROM employee";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<h2>Employees</h2>";
    echo "<table border='1'>";
    echo "<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Middle Name</th><th>Birthday</th><th>Address</th></tr>";
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["id"] . "</td>";
        echo "<td>" . $row["first_name"] . "</td>";
        echo "<td>" . $row["last_name"] . "</td>";
        echo "<td>" . $row["middle_name"] . "</td>";
        echo "<td>" . $row["birthday"] . "</td>";
        echo "<td>" . $row["address"] . "</td>";
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

// Update
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['update'])) {
    $employee_id = $_POST['employee_id'];
    $new_address = $_POST['new_address'];

    $sql = "UPDATE employee SET address = '$new_address' WHERE id = $employee_id";

    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }
}

// Delete
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['delete'])) {
    $employee_id = $_POST['employee_id'];

    $sql = "DELETE FROM employee WHERE id = $employee_id";

    if ($conn->query($sql) === TRUE) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $conn->error;
    }
}
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['id'])) {
    $employee_id = $_POST['id'];

    $sql = "SELECT * FROM employee WHERE id = $employee_id";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        echo "<h2>Employee Details</h2>";
        echo "<table border='1'>";
        echo "<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Middle Name</th><th>Birthday</th><th>Address</th></tr>";
        $row = $result->fetch_assoc();
        echo "<tr>";
        echo "<td>" . $row["id"] . "</td>";
        echo "<td>" . $row["first_name"] . "</td>";
        echo "<td>" . $row["last_name"] . "</td>";
        echo "<td>" . $row["middle_name"] . "</td>";
        echo "<td>" . $row["birthday"] . "</td>";
        echo "<td>" . $row["address"] . "</td>";
        echo "</tr>";
        echo "</table>";
    } else {
        echo "Employee not found with ID: $employee_id";
    }
}

$conn->close();

?>

<!-- Create form -->
<h2>Create Employee</h2>
<form method="POST">
    <label for="first_name">First Name:</label>
    <input type="text" id="first_name" name="first_name"><br>
    <label for="last_name">Last Name:</label>
    <input type="text" id="last_name" name="last_name"><br>
    <label for="middle_name">Middle Name:</label>
    <input type="text" id="middle_name" name="middle_name"><br>
    <label for="birthday">Birthday:</label>
    <input type="date" id="birthday" name="birthday"><br>
    <label for="address">Address:</label>
    <input type="text" id="address" name="address"><br>
    <button type="submit" name="create">Add Employee</button>
</form>

<!-- Update form -->
<h2>Update Employee</h2>
<form method="POST">
    <label for="employee_id">Employee ID:</label>
    <input type="text" id="employee_id" name="employee_id"><br>
    <label for="new_address">New Address:</label>
    <input type="text" id="new_address" name="new_address"><br>
    <button type="submit" name="update">Update Address</button>
</form>

<!-- Delete form -->
<h2>Delete Employee</h2>
<form method="POST">
    <label for="employee_id">Employee ID:</label>
    <input type="text" id="employee_id" name="employee_id"><br>
    <button type="submit" name="delete">Delete Employee</button>
</form>

<h2>Search Employee by ID</h2>
<form method="POST">
    <label for="id">Employee ID:</label>
    <input type="text" id="id" name="id">
    <button type="submit">Search</button>
</form>

</html>
